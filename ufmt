#!/usr/bin/env python3

from __future__ import print_function

import sys, argparse

def print_para(para, outp, width):
	linelen = 0
	if para:
		for word in para:
			if linelen:
				if linelen + len(word) > width:
					outp.write("\n")
					linelen = 0
				else:
					outp.write(" ")
					linelen+= 1
			outp.write(word)
			linelen+= len(word)
		outp.write("\n")

def reflow(inp, outp, width):
	para = []
	for line in inp:
		words = line.split()
		if words:
			para.extend(words)
		else:
			print_para(para, outp, width)
			outp.write("\n")
			para = []
	print_para(para, outp, width)

width = 75
first_arg = 1
# process unconventional width option
if len(sys.argv)>=2 \
	and sys.argv[1].startswith("-") \
	and sys.argv[1][1:].isdigit():
	width = int(sys.argv[1][1:])
	first_arg = 2

# now use argparse to process the rest of arguments
parser = argparse.ArgumentParser(description="fmt(1) with Unicode support")
parser.add_argument("-c", "--crown-margin",  action="store_true",
			help="preserve indentation of first two lines")
parser.add_argument("-p", "--prefix", 
			help="reformat  only lines beginning with STRING,"
				   + " reattaching the prefix to reformatted lines")
parser.add_argument("-s", "--split-only", action="store_true",
			help="split long lines, but do not refill")
parser.add_argument("-t", "--tagged-paragraph", action="store_true",
			help="indentation of first line different from second")
parser.add_argument("-u", "--uniform-spacing", action="store_true",
			help="one space between words, two after sentences")
parser.add_argument("-w", "--width", type=int, default=width,
			help="maximum line width (default of 75 columns)")
parser.add_argument("-g", "--goal", type=int, default=None,
			help="goal width (default of 93%% of width)")
parser.add_argument("--version", action="store_const",
			const="ufmt 0.0 by David Tolpin, BSD License",
			help="output version information and exit")
parser.add_argument("files", nargs="*",
			help="names of input files")
args = parser.parse_args(sys.argv[first_arg:])

if args.version is not None:
	print(args.version)
	sys.exit(0)

if args.goal is None:
	args.goal = round(args.width*0.93)
if not args.files:
	args.files = ["-"]

for file in args.files:
	if file=="-":
		reflow(sys.stdin, sys.stdout, args.width)
	else:
		with open(file) as inp:
			reflow(inp, sys.stdout, args.width)
